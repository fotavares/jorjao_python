# -*- coding: utf-8 -*-
import time
import random
import telepot
import json
from telepot.loop import MessageLoop
import emoji
import sys
import logging
import constants
import datetime

def find_words(text, search):
	"""Find exact words"""
	dText   = text.split()
	dSearch = search.split()

	found_word = 0

	for text_word in dText:
		for search_word in dSearch:
			if search_word == text_word:
				found_word += 1

	if found_word == len(dSearch):
		return True
	else:
		return False

def handle(msg):
	content_type, chat_type, chat_id = telepot.glance(msg)

	reload(sys)
	sys.setdefaultencoding('utf-8')
	command = ''
	new_user = ''
	user_left = ''
	reply_to = ''

	#controle porco de horas pra resposta do bom dia
	hora = int(datetime.datetime.now().strftime('%H')) - 3 #Horario BR
	dia = hora >= 4 and hora < 12
	tarde = hora >= 5 and hora < 18
	noite = hora >= 0 and hora < 4 or hora >= 18 and hora < 24

	if chat_type == 'private':
		#print(msg)
		if 'text' in msg:
			command = msg['text'].encode('utf-8')
			if '/relay' in command.lower():
				bot.sendMessage(constants.GRUPO_JORJAO, '%s' % command[7:])
			if '/fala' in command.lower():
				bot.sendMessage(constants.GRUPO_JORJAO, '%s' % command[6:])
			logging.info('{}({}): {}\n'.format(msg['from']['first_name'],msg['from']['username'], command))
			return

		if 'photo' in msg:
			if msg['from']['username'] == "Fotavares":
				bot.sendMessage(chat_id, msg["photo"][0]["file_id"])



	if content_type == 'new_chat_member':
		new_users = msg['new_chat_members']
		for new_user in new_users:
			nuser = new_user['username']
			name = new_user['first_name']
			if nuser != constants.USER_JORJAO:
				bot.sendMessage(chat_id, 'Falae %s! Suave?' % name)
				logging.info('{}({}) entrou\n'.format(name,nuser))
			else:
				bot.sendMessage(chat_id, 'Cheguei putada!!!!!!!')

	if content_type == 'left_chat_member':
		user_left = msg['left_chat_member']['username'].encode('utf-8')
		name = msg['left_chat_member']['first_name'].encode('utf-8')
		if user_left != constants.USER_JORJAO:
			bot.sendMessage(chat_id, emoji.emojize(':skull:') + ' RIP %s ' + emoji.emojize(':skull:') % name)
			logging.info('{}({}) saiu\n'.format(name,nuser))

	if content_type == 'text':
		command = msg['text'].encode('utf-8')
		reply_id = msg['message_id']
		if 'reply_to_message' in msg:
			reply_to = msg['reply_to_message']['from']['username']

		if msg['from']['username'] == "tjader" and command == 'Oi.':
			bot.sendMessage(chat_id, "Oi gato, quer sorvete?"+emoji.emojize(":icecream:")+emoji.emojize(":ice_cream:") ,reply_to_message_id=reply_id)
		#if msg['from']['username'] == "sillyx":
    	#		bot.sendMessage(chat_id, command ,reply_to_message_id=reply_id)

		command = command.lower()
		if 'jorjao' in command or 'jorjão' in command or reply_to == constants.USER_JORJAO:
			if find_words(command,'manda nudes'):
				resp = [u"Mais tarde",u"Tah cedo ainda",u"Hoje não",u"Agora não",
						u"Issaki eh grupo de familia, xovem",u"NEVAR",u"Nops",
						u"Um dia quem sabe",u"Vai sonhando", u"Sem vergonha",u"Sai exú"]
				text = random.choice(resp)
				bot.sendMessage(chat_id, text,reply_to_message_id=reply_id,parse_mode='html')

		if 'pinto' in command or 'benes' in command:
			resp = [':baby_chick:', ':hatched_chick:', ':hatching_chick:']
			bot.sendMessage(chat_id, emoji.emojize(random.choice(resp)),reply_to_message_id=reply_id)

		if 'comprar' in command:
			bot.sendMessage(chat_id, emoji.emojize(':money_with_wings:'),reply_to_message_id=reply_id)

		if find_words(command,'top') or find_words(command,'tope') or find_words(command,'topi'):
			msg_top = emoji.emojize(":top:",use_aliases=True)
			resp = ["Topster","Topissimo","Topzeira","Topeira","HeliTOPtero","Topson",msg_top]
			bot.sendMessage(chat_id, msg_top + random.choice(resp) + msg_top,reply_to_message_id=reply_id,parse_mode="HTML")

		if find_words(command,'sacanagem'):
			msg_frango = emoji.emojize(":chicken:",use_aliases=True)
			bot.sendMessage(chat_id, " Sacanagem boa é no @solteafranga " + msg_frango + msg_frango + msg_frango,reply_to_message_id=reply_id,parse_mode='HTML')

		if 'brotheragem' in command or 'broderagem' in command or 'broderagi'in command:
			bot.sendMessage(chat_id, "https://www.campograndenews.com.br/lado-b/comportamento-23-08-2011-08/grupo-cria-festa-para-homens-que-curtem-homens-mas-que-garantem-ser-heteros")

		if 'nudes' in command:
			resp=["Eu ouvi NUDES??","Nudes? Adoro!","Nudes? Também quero!"]
			bot.sendMessage(chat_id, random.choice(resp),reply_to_message_id=reply_id)

		if 'bom dia' in command:
			caption = ""
			if tarde:
				caption = "Você quis dizer boa tarde, né? Mas OK..."
			if noite:
				caption = "Não seria boa noite? Como sei que não posso te contrariar por ordens médica, tomae..."

			resp = [u'AgADAQAD9KcxG1_QoEd-KYpEFd2PBMkZ9y8ABOMGIbdR8jYIqPwCAAEC',
					u'AgADBAAD0KcxG7mtVVMF9H3VVaLmpHxzmhoABAIgSitWRJgij10BAAEC',
					u'AgADBAAD6qcxG0U07VJ3Yk9Johked3fjkBkABHA7R7ssMZpn46UBAAEC',
					u'AgADBAAD0KcxG0Y8hVNk175-My-yNAtsmhoABFjlqrqtpBWXlHMBAAEC',
					u'AgADBAADwacxG1bFrVNK8Kq_-649T0IOjhoABBXjI3MkN7XEhTwFAAEC',
					u'AgADBAADtacxG7bjrFPVX5Xs-C29AUpaiRoABF4feij7kxiy4mYFAAEC',
					u'AgADAQAD5KcxG5szsUdt9fKhRA2hlH10DDAABMDmLAeLs2wy1mcBAAEC',
					u'AgADAQAD2qcxG4NoqEcR1aMKYxZBFMN1DDAABPruoIaB1ybwB2kBAAEC',
					u'AgADAQAD26cxG4NoqEcywOn40QRw87UrAzAABPKDG6Tqg9UgkTwCAAEC',
					u'AgADAQAD5acxG5szsUdE2JzXW5jgOFYmAzAABBHo4qS4wgAB8sJfAgABAg',
					u'AgADAQAD5qcxG5szsUcyKdGvwk0DPGRuDDAABL2lrHp7sFueHWUBAAEC',
					u'AgADAQAD3KcxG4NoqEfzgfrvEo0yyvzrCjAABPi2Pl6HVaHnukkAAgI',
					u'AgADAQAD56cxG5szsUdjqNR4K6CMUdwmAzAABO-Qe_FNeRxPC14CAAEC']
			foto = random.choice(resp)
			bot.sendPhoto(chat_id, foto,reply_to_message_id=reply_id,caption=caption)

		if 'boa tarde' in command:
			caption = ""
			if dia:
				caption = "Se já deu boa tarde essa hora, é porque vai dar migué e sumir do mapa, vai vendo..."
			if noite:
				caption = "Já tá de noite, porra"
			resp = [u'AgADBAAD_8Y9G9sXZAdXNQ4av0-1S4YOkRkABLawNDOJPmb9OjsAAgI',
					u'AgADBAADuFo8G1cYZAduG6Wn6rtFI66IjBoABLvaFtqFpY9DoTEBAAEC',
					u'AgADBAAD3qcxGxH61FKAs0pfziYrfFJQiRoABWhPgAbRQkl12gQAAQI',
					u'AgADBAADHOc5G1YYZAejCnMwRkqHPwAB5-EZAASjSzy1UpWQDOnJAgABAg',
					u'AgADBAAD4acxG0m6pFNagdchIT7LcuS7mxoABDbpBgEl0O7AhokBAAEC',
					u'AgADAQAD_qcxGzrgqEcwBou7k-oWVFcd9y8ABDjKOIVtSn6_CvwCAAEC',
					u'AgADAQAD3acxG4NoqEfovOimdEuFGHEg9y8ABPkOH-5f-gnxAgEDAAEC',
					u'AgADAQAD3qcxG4NoqEenvrSkEgmwHXAc9y8ABFDjc9ocyso4nPsCAAEC',
					u'AgADAQAD36cxG4NoqEdkkTV7RX7mb0hhDDAABPtQO6FkKtiblGcBAAEC',
					u'AgADAQAD_6cxGzrgqEdtgKEqi9wa4F1nAzAABKhzbBmNMtukykkAAgI',
					u'AgADAQAEqDEbOuCoRw46V7cYWCElbrkKMAAENfnd-xtnpLx_lQACAg',
					u'AgADAQAD4KcxG4NoqEc6NudrIq6zvBa9CjAABCthK1oRMzAhe5IAAgI']
			foto = random.choice(resp)
			bot.sendPhoto(chat_id, foto,reply_to_message_id=reply_id,caption=caption)

		if 'boa noite' in command or 'boa noitche' in command:
			caption = ""
			if dia:
				caption = "Tu tá na australia por acaso? Que porra é essa?"
			if tarde:
				caption = "Já vai dormir? Quer porra nenhuma hein!"

			resp = [u'AgADBAAD3qcxG4nAbVKFFb5oqfjHYWjYjRoABJj7Z4h4e_n5SFwEAAEC',
					u'AgADBAADM4Y5GwgXZAcDJLmXRpOD7Gwp4xkABE_80thyRuncILsBAAEC',
					u'AgADBAADSeY5G90bZAeaUmjQtLs2uNn44RkABMZmN4ROskBzV-QCAAEC',
					u'AgADBAADGwQ-G3kdZAcS2SAfPRMawQ0bjhoABIp-BR0ke02cNrMDAAEC',
					u'AgADBAADy6cxG4LddVLL0w9z5uHFu6gHkRkABNKfT9H_GIIX3SkBAAEC',
					u'AgADAQADAagxGzrgqEfq_K-CQPhzG5JoAzAABKbCnSpI4Akaa0cAAgI',
					u'AgADAQADAqgxGzrgqEfWngHm5qqjgncd9y8ABF23aeSUQgagrfgCAAEC',
					u'AgADAQAD4acxG4NoqEdCFsygZag33o0lAzAABE3ayzW1NXEpz1cCAAEC',
					u'AgADAQADA6gxGzrgqEdt2TcmSfYODeSvCjAABDwcR9zvFuGjMpoAAgI',
					u'AgADAQADBKgxGzrgqEeozrGrNB3qmWcrAzAABJ8allo7n895yjsCAAEC',
					u'AgADAQAD4qcxG4NoqEeWK0SM3L2c4HVlDDAABEyKcoXL935PemkBAAEC',
					u'AgADAQADBagxGzrgqEd3Xxo5hBMUPXksAzAABL3QluAf8yxHnjkCAAEC']
			foto = random.choice(resp)
			bot.sendPhoto(chat_id, foto,reply_to_message_id=reply_id,caption=caption)

		if 'brojob' in command or 'bro-job' in command or 'bro job' in command:
			bot.sendPhoto(chat_id,u'AgADAQADyqcxG4GcmEcZfdMVbM4bpT5vDDAABE1LLLDl2QGbxmUBAAEC')

logging.basicConfig(filename='chat.jorjao.log',level=logging.INFO)

bot = telepot.Bot(constants.TOKEN_JORJAO)

MessageLoop(bot, handle).run_as_thread()
print ('I am listening ...')

while 1:
	time.sleep(10)
